#!/usr/bin/env python3

import os
import os.path
import subprocess
from shutil import copy2, copytree

VimFolder = '.vim'

StartInstall = input('Using this installer will overwrite your vimrc, continue? (y/N):')

if StartInstall[0].upper() == 'Y':

    HomeDir = os.path.expanduser('~') + '/'
    if HomeDir !=  '':
        print('Installing...')
        try:
            if os.path.isfile(HomeDir + '.vimrc'):
                    copy2(HomeDir + '.vimrc', HomeDir + '.vimrc.bak')
                    print(HomeDir + '.vimrc backed up as .vimrc.bak')
            copy2('.vimrc', HomeDir + '.vimrc')
            print('New vimrc copied')
        except Exception as inst:
            print(str(inst))
        if os.path.exists(HomeDir + VimFolder + '/'):
            print(HomeDir + '.vim folder found, copying colors...')
            try:
                if os.path.exists(HomeDir + VimFolder + '/colors/'):
                    ColorFiles = os.listdir('colors/')
                    for File in ColorFiles:
                        copy2('colors/' + File, HomeDir + VimFolder + '/colors/')
                else:
                    copytree('colors/', HomeDir + VimFolder + '/colors/')
                print('colors copied.')
                
                print('copying after/ folder...')
                if os.path.exists(HomeDir + VimFolder + '/after/plugin/'):
                    AfterPlug = os.listdir('after/plugin/')
                    for File in AfterPlug:
                        copy2('after/plugin/' + File, HomeDir + VimFolder + '/after/plugin/')
                else:
                    copytree('after/', HomeDir + VimFolder + '/after/')
                print('after plugins copied')

                if not os.path.exists('Plugins/'):
                    print('Deflating plugins archive...')
                    if os.name == 'posix':
                        subprocess.run(['unzip','Plugins.zip'])
                    else:
                        subprocess.run(['7z','x','Plugins.zip'])
                    print('Copying plugins into ' + HomeDir + VimFolder + 
                          '/pack/MyPacks/start/ ...')
                print('Copying pack folder...')
                if not os.path.exists(HomeDir + VimFolder + '/pack/MyPacks/start/'):
                    copytree('Plugins/', HomeDir + VimFolder + '/pack/MyPacks/start/')
                else:
                    Plugins = os.listdir('Plugins/')
                    for Dirs in Plugins:
                        copytree('Plugins/' + Dirs, HomeDir + VimFolder + 
                              '/pack/MyPacks/start/' + Dirs)
                print('Plugins copied. Done.')
            except Exception as inst:
                print(str(inst))
        else:
            print(".vim folder doesn't exist, verify Vim is intalled")
raise SystemExit
