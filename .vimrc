"" === General ===

set backspace=indent,eol,start
set colorcolumn=80
set number                        " Numbers for lines
set history=1000                  
autocmd CompleteDone * pclose     " Auto close preview split
syntax on
filetype indent plugin on

"" === Search ===

set ignorecase      " Ignore case
set smartcase       " Unless it starts with Capital letter
"set hlsearch        " Highlight search result

"" === Cosmetic ===

set termguicolors
color ayucust
let g:airline_theme='papercolor'
let g:airline_powerline_fonts = 1

"" === Keys mapping ===

" Clear highlighted searches by pressing ESC
"nnoremap <esc> :nohlsearch<return><esc>
"nnoremap <esc>^[ <esc>^[
"

map <F9> :w \| :!python main.py<CR>
imap <F9> <esc> :w \| :!python main.py<CR>

"make < > shifts keep selection
vnoremap < <gv
vnoremap > >gv
"

"" === Misc ===

au BufRead,BufNewFile *.kv set filetype=python    " Use python syntax bor kivy

"" === Diff mode specific ===

if &diff
	set diffopt+=icase
	set colorcolumn&
	nnoremap - [c
	nnoremap + ]c
"	color codeburn      " Uncomment if terminal doesn't like ayucust
endif

